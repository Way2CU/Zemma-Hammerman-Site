<?php
/**
 * Custom Caracal Mailer
 *
 * Author: Vladimir Anusic
 */

namespace Modules\CustomMailer;
use Modules\SendGrid\Mailer;


class SendgridCustomMailer extends Mailer {

    /**
     * Validate user captcha token using Google service.
     *
     * @param string $token
     * @return array $result
     */
    public function validateRecaptcha($token) {
        global $secret_key;
        $service_url = 'https://www.google.com/recaptcha/api/siteverify';
        $fields_string = '';
        $fields = array(
            'secret' => $secret_key,
            'response' => $token
        );

        foreach($fields as $key => $value) {
            $fields_string .= $key.'='.$value.'&';
        }
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $service_url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    public function send() {
        $response = $this->validateRecaptcha($_POST['captcha-token']);

        if($response['success'])
        	return parent::send(); else
        	return false;
    }
}
?>
