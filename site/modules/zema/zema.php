<?php

/**
 * Zema Hammerman API Support
 *
 * This module submits data sent by contact form to API
 *
 * Author: Mladen Mijatov
 */
use Core\Module;
use Core\Events;


class zema extends Module {
	private static $_instance;

	/**
	 * Initial parameters for sending to API. Since client takes absolutely
	 * no effort in protecting data (no HTTPS, no authentication, no encryption)
	 * there's no need for us to try and protect them either. Their approach to
	 * security is whitelisting originating IP. Good luck with that chaps!
	 *
	 * @var array
	 */
	private $data = array(
		'פארק צפון'            => array(),
		'אכזיב נהריה'          => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 5544,
				'Password'     => 'arsiv1306'
			),
		'אמירי נוף'            => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 5473,
				'Password'     => 'ami1306'
			),
		'בצלאל תל-אביב'        => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 3778,
				'Password'     => 'bet1306'
			),
		'כרמיאלה הדור הבא'     => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 8831,
				'Password'     => 'zxc27022020'
			),
		'נופיה בית שמש'        => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 4013,
				'Password'     => 'nof1306'
			),
		'פרויקט קריניצי החדשה' => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 4440,
				'Password'     => 'kar1306'
			),
		'לב גני תקוה'          => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 6799,
				'Password'     => 'gan1306'
			),
		'סביוני גני תקווה'     => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 8640,
				'Password'     => 'qwe080719'
			),
		'פרויקט אנדרומדה'      => array(
				'MediaTitle'   => 'אתר הבית- צמח המרמן',
				'ProjectID'    => 8390,
				'Password'     => 'zxc16042020'
			),
	);

	private $ctm_form_id = 'FRT472ABB2C5B9B141A65088C9D9EE12FB66ADBCA8DB4E7D8E3E4C5ED307AB450E4';

	/**
	 * API field to contact form field name map.
	 * @var array
	 */
	private $field_map = array(
			'Phone' => 'phone',
			'Email' => 'mail'
		);

	/**
	 * Projects to skip submission to BMBY CRM.
	 * @var array
	 */
	private $ignore_bmby = array('נופיה בית שמש', 'פארק צפון');

	/**
	 * Projects to skip submission to CTM.
	 * @var array
	 */
	private $ignore_ctm = array();

	const API_ENDPOINT = 'http://www.bmby.com/shared/AddClient/index.php';
	const API2_ENDPOINT = 'https://api.calltrackingmetrics.com/api/v1/formreactor/';

	/**
	 * Constructor
	 */
	protected function __construct() {
		global $section;
		parent::__construct(__FILE__);

		Events::connect('contact_form', 'submitted', 'handle_submit', $this);
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance() {
		if (!isset(self::$_instance))
			self::$_instance = new self();

		return self::$_instance;
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transfer_control($params = array(), $children = array()) {
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function initialize() {
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function cleanup() {
	}

	/**
	 * Submit data to client's CRM.
	 *
	 * @param array $sender
	 * @param array $recipient
	 * @param array $template
	 * @param array $submit_data
	 */
	private function submit_bmby($sender, $recipient, $template, $submit_data) {
		$project_name = null;

		// get project name from global form
		if (isset($submit_data['projects'])) {
			$project_name = $submit_data['projects'];
			$full_name = $submit_data['name'];

		// get project name from project contact form
		} else if (isset($submit_data['name'])) {
			$project_name = $submit_data['name'];
			$full_name = $submit_data['fullname'];
		}

		// make sure project is defined
		if (is_null($project_name) || !array_key_exists($project_name, $this->data)) {
			trigger_error('No project specified or missing configuration.', E_USER_WARNING);
			return;
		}

		if (in_array($project_name, $this->ignore_bmby))
			return;

		// create content for sending
		$content = $this->data[$project_name];
		foreach ($this->field_map as $api_field => $form_field)
			if (array_key_exists($form_field, $submit_data))
				$content[$api_field] = $submit_data[$form_field];

		$name = explode(' ', $full_name, 2);
		$content['Fname'] = $name[0];
		if (count($name) > 1)
			$content['Lname'] = $name[1]; else
			$content['Lname'] = '';

		// make api call
		$handle = curl_init(self::API_ENDPOINT);
		curl_setopt($handle, CURLOPT_POST, true);
		curl_setopt($handle, CURLOPT_HEADER, false);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		/* curl_setopt($handle, CURLOPT_HTTPHEADER, $headers); */
		curl_setopt($handle, CURLOPT_POSTFIELDS, $content);
		curl_setopt($handle, CURLOPT_USERAGENT, 'Caracal '._VERSION);
		$response = curl_exec($handle);
		curl_close($handle);

		if ($response === FALSE || $response == '0')
			error_log('Failed submission for `'.$project_name.'` on API.', E_USER_NOTICE);
	}

	/**
	 * Submit data to CTM.
	 *
	 * @param array $sender
	 * @param array $recipient
	 * @param array $template
	 * @param array $submit_data
	 */
	private function submit_ctm($sender, $recipient, $template, $submit_data) {
		global $ctm_access_key, $ctm_secret_key;
		$project_name = null;

		// get project name from global form
		if (isset($submit_data['projects'])) {
			$project_name = $submit_data['projects'];
			$full_name = $submit_data['name'];

		// get project name from project contact form
		} else if (isset($submit_data['name'])) {
			$project_name = $submit_data['name'];
			$full_name = $submit_data['fullname'];
		}

		if (in_array($project_name, $this->ignore_ctm))
			return;

		$phone_number = preg_replace(';0?(.*);', '\1', $submit_data['phone']);
		$payload = array(
			'unique_form_id'      => $this->ctm_form_id,
			'phone_number'        => $phone_number,
			'country_code'        => '+972',
			'caller_name'         => $full_name,
			'email'               => $submit_data['mail'],
			'visitor_sid'         => $submit_data['sid'],
			'custom_project_name' => $project_name
		);

		$ctm_token = base64_encode($ctm_access_key.':'.$ctm_secret_key);
		$options = array('http' => array(
			'method'  => 'POST',
			'header'  => "Authorization: Basic {$ctm_token}\r\nContent-type: application/json",
			'content' => json_encode($payload)
		));

		$context = stream_context_create($options);
		$result = file_get_contents(self::API2_ENDPOINT.$this->ctm_form_id, false, $context);

		return json_decode($result);
	}

	/**
	 * Handle contact form submission event.
	 *
	 * @param array $sender
	 * @param array $recipient
	 * @param array $template
	 * @param array $submit_data
	 */
	public function handle_submit($sender, $recipient, $template, $submit_data) {
		$this->submit_bmby($sender, $recipient, $template, $submit_data);
		$this->submit_ctm($sender, $recipient, $template, $submit_data);
	}
}

?>
