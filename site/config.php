<?php

/**
 * Site Configuration File
 *
 * This file overrides properties defined in main configuration
 * file for Caracal located in `units/config.php`.
 */

use Core\CSP;
use Core\Cache\Type as CacheType;
use Core\Session\Type as SessionType;

// document standard
define('_TIMEZONE', 'America/New_York');

define('DEBUG', 1);
// define('SQL_DEBUG', 1);

// site language configuration
$available_languages = array('he', 'en', 'fr', 'ru');
$default_language = 'he';

// default session options
$session_type = SessionType::BROWSER;

// database
$db_type = DatabaseType::MYSQL;
$db_config = array(
		'host' => 'localhost',
		'user' => 'root',
		'pass' => 'caracal',
		'name' => 'web_engine'
	);

// allow loading scripts from different domain
/* CSP\Parser::add_value(CSP\Element::SCRIPTS, 'www.consolto.com'); */
/* CSP\Parser::add_value(CSP\Element::SCRIPTS, 'client.consolto.com'); */
/* CSP\Parser::add_value(CSP\Element::SCRIPTS, 'domain.com'); */

// force secure connections
$force_https = true;

// configure code generation
$cache_method = Core\Cache\Type::NONE;
$optimize_code = false;
$include_styles = true;
$url_rewrite = true;

$sendgrid_api_key = '';
$secret_key = '';
$ctm_access_key = '';
$ctm_secret_key = '';

?>
